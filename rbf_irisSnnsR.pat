SNNS pattern definition file V4.2
generated at Tue Nov 15 23:16:11 2016


No. of patterns : 150
No. of input units : 4
No. of output units : 3

# Input pattern 1:
-0.897674 1.0156 -1.33575 -1.31105 
# Output pattern 1:
0.9 0.1 0.1 
# Input pattern 2:
-1.1392 -0.131539 -1.33575 -1.31105 
# Output pattern 2:
0.9 0.1 0.1 
# Input pattern 3:
-1.38073 0.327318 -1.3924 -1.31105 
# Output pattern 3:
0.9 0.1 0.1 
# Input pattern 4:
-1.50149 0.0978893 -1.2791 -1.31105 
# Output pattern 4:
0.9 0.1 0.1 
# Input pattern 5:
-1.01844 1.24503 -1.33575 -1.31105 
# Output pattern 5:
0.9 0.1 0.1 
# Input pattern 6:
-0.535384 1.93331 -1.16581 -1.04867 
# Output pattern 6:
0.9 0.1 0.1 
# Input pattern 7:
-1.50149 0.786174 -1.33575 -1.17986 
# Output pattern 7:
0.9 0.1 0.1 
# Input pattern 8:
-1.01844 0.786174 -1.2791 -1.31105 
# Output pattern 8:
0.9 0.1 0.1 
# Input pattern 9:
-1.74302 -0.360967 -1.33575 -1.31105 
# Output pattern 9:
0.9 0.1 0.1 
# Input pattern 10:
-1.1392 0.0978893 -1.2791 -1.44224 
# Output pattern 10:
0.9 0.1 0.1 
# Input pattern 11:
-0.535384 1.47446 -1.2791 -1.31105 
# Output pattern 11:
0.9 0.1 0.1 
# Input pattern 12:
-1.25996 0.786174 -1.22246 -1.31105 
# Output pattern 12:
0.9 0.1 0.1 
# Input pattern 13:
-1.25996 -0.131539 -1.33575 -1.44224 
# Output pattern 13:
0.9 0.1 0.1 
# Input pattern 14:
-1.86378 -0.131539 -1.50569 -1.44224 
# Output pattern 14:
0.9 0.1 0.1 
# Input pattern 15:
-0.0523308 2.16274 -1.44905 -1.31105 
# Output pattern 15:
0.9 0.1 0.1 
# Input pattern 16:
-0.173094 3.08046 -1.2791 -1.04867 
# Output pattern 16:
0.9 0.1 0.1 
# Input pattern 17:
-0.535384 1.93331 -1.3924 -1.04867 
# Output pattern 17:
0.9 0.1 0.1 
# Input pattern 18:
-0.897674 1.0156 -1.33575 -1.17986 
# Output pattern 18:
0.9 0.1 0.1 
# Input pattern 19:
-0.173094 1.70389 -1.16581 -1.17986 
# Output pattern 19:
0.9 0.1 0.1 
# Input pattern 20:
-0.897674 1.70389 -1.2791 -1.17986 
# Output pattern 20:
0.9 0.1 0.1 
# Input pattern 21:
-0.535384 0.786174 -1.16581 -1.31105 
# Output pattern 21:
0.9 0.1 0.1 
# Input pattern 22:
-0.897674 1.47446 -1.2791 -1.04867 
# Output pattern 22:
0.9 0.1 0.1 
# Input pattern 23:
-1.50149 1.24503 -1.56234 -1.31105 
# Output pattern 23:
0.9 0.1 0.1 
# Input pattern 24:
-0.897674 0.556746 -1.16581 -0.917474 
# Output pattern 24:
0.9 0.1 0.1 
# Input pattern 25:
-1.25996 0.786174 -1.05251 -1.31105 
# Output pattern 25:
0.9 0.1 0.1 
# Input pattern 26:
-1.01844 -0.131539 -1.22246 -1.31105 
# Output pattern 26:
0.9 0.1 0.1 
# Input pattern 27:
-1.01844 0.786174 -1.22246 -1.04867 
# Output pattern 27:
0.9 0.1 0.1 
# Input pattern 28:
-0.776911 1.0156 -1.2791 -1.31105 
# Output pattern 28:
0.9 0.1 0.1 
# Input pattern 29:
-0.776911 0.786174 -1.33575 -1.31105 
# Output pattern 29:
0.9 0.1 0.1 
# Input pattern 30:
-1.38073 0.327318 -1.22246 -1.31105 
# Output pattern 30:
0.9 0.1 0.1 
# Input pattern 31:
-1.25996 0.0978893 -1.22246 -1.31105 
# Output pattern 31:
0.9 0.1 0.1 
# Input pattern 32:
-0.535384 0.786174 -1.2791 -1.04867 
# Output pattern 32:
0.9 0.1 0.1 
# Input pattern 33:
-0.776911 2.39217 -1.2791 -1.44224 
# Output pattern 33:
0.9 0.1 0.1 
# Input pattern 34:
-0.414621 2.6216 -1.33575 -1.31105 
# Output pattern 34:
0.9 0.1 0.1 
# Input pattern 35:
-1.1392 0.0978893 -1.2791 -1.31105 
# Output pattern 35:
0.9 0.1 0.1 
# Input pattern 36:
-1.01844 0.327318 -1.44905 -1.31105 
# Output pattern 36:
0.9 0.1 0.1 
# Input pattern 37:
-0.414621 1.0156 -1.3924 -1.31105 
# Output pattern 37:
0.9 0.1 0.1 
# Input pattern 38:
-1.1392 1.24503 -1.33575 -1.44224 
# Output pattern 38:
0.9 0.1 0.1 
# Input pattern 39:
-1.74302 -0.131539 -1.3924 -1.31105 
# Output pattern 39:
0.9 0.1 0.1 
# Input pattern 40:
-0.897674 0.786174 -1.2791 -1.31105 
# Output pattern 40:
0.9 0.1 0.1 
# Input pattern 41:
-1.01844 1.0156 -1.3924 -1.17986 
# Output pattern 41:
0.9 0.1 0.1 
# Input pattern 42:
-1.62225 -1.73754 -1.3924 -1.17986 
# Output pattern 42:
0.9 0.1 0.1 
# Input pattern 43:
-1.74302 0.327318 -1.3924 -1.31105 
# Output pattern 43:
0.9 0.1 0.1 
# Input pattern 44:
-1.01844 1.0156 -1.22246 -0.786281 
# Output pattern 44:
0.9 0.1 0.1 
# Input pattern 45:
-0.897674 1.70389 -1.05251 -1.04867 
# Output pattern 45:
0.9 0.1 0.1 
# Input pattern 46:
-1.25996 -0.131539 -1.33575 -1.17986 
# Output pattern 46:
0.9 0.1 0.1 
# Input pattern 47:
-0.897674 1.70389 -1.22246 -1.31105 
# Output pattern 47:
0.9 0.1 0.1 
# Input pattern 48:
-1.50149 0.327318 -1.33575 -1.31105 
# Output pattern 48:
0.9 0.1 0.1 
# Input pattern 49:
-0.656147 1.47446 -1.2791 -1.31105 
# Output pattern 49:
0.9 0.1 0.1 
# Input pattern 50:
-1.01844 0.556746 -1.33575 -1.31105 
# Output pattern 50:
0.9 0.1 0.1 
# Input pattern 51:
1.39683 0.327318 0.533621 0.26326 
# Output pattern 51:
0.1 0.9 0.1 
# Input pattern 52:
0.672249 0.327318 0.420326 0.394453 
# Output pattern 52:
0.1 0.9 0.1 
# Input pattern 53:
1.27607 0.0978893 0.646916 0.394453 
# Output pattern 53:
0.1 0.9 0.1 
# Input pattern 54:
-0.414621 -1.73754 0.137087 0.132067 
# Output pattern 54:
0.1 0.9 0.1 
# Input pattern 55:
0.793012 -0.590395 0.476973 0.394453 
# Output pattern 55:
0.1 0.9 0.1 
# Input pattern 56:
-0.173094 -0.590395 0.420326 0.132067 
# Output pattern 56:
0.1 0.9 0.1 
# Input pattern 57:
0.551486 0.556746 0.533621 0.525645 
# Output pattern 57:
0.1 0.9 0.1 
# Input pattern 58:
-1.1392 -1.50811 -0.259446 -0.261511 
# Output pattern 58:
0.1 0.9 0.1 
# Input pattern 59:
0.913776 -0.360967 0.476973 0.132067 
# Output pattern 59:
0.1 0.9 0.1 
# Input pattern 60:
-0.776911 -0.819823 0.0804397 0.26326 
# Output pattern 60:
0.1 0.9 0.1 
# Input pattern 61:
-1.01844 -2.42582 -0.146151 -0.261511 
# Output pattern 61:
0.1 0.9 0.1 
# Input pattern 62:
0.0684325 -0.131539 0.250383 0.394453 
# Output pattern 62:
0.1 0.9 0.1 
# Input pattern 63:
0.189196 -1.96696 0.137087 -0.261511 
# Output pattern 63:
0.1 0.9 0.1 
# Input pattern 64:
0.309959 -0.360967 0.533621 0.26326 
# Output pattern 64:
0.1 0.9 0.1 
# Input pattern 65:
-0.293857 -0.360967 -0.0895033 0.132067 
# Output pattern 65:
0.1 0.9 0.1 
# Input pattern 66:
1.03454 0.0978893 0.363678 0.26326 
# Output pattern 66:
0.1 0.9 0.1 
# Input pattern 67:
-0.293857 -0.131539 0.420326 0.394453 
# Output pattern 67:
0.1 0.9 0.1 
# Input pattern 68:
-0.0523308 -0.819823 0.193735 -0.261511 
# Output pattern 68:
0.1 0.9 0.1 
# Input pattern 69:
0.430722 -1.96696 0.420326 0.394453 
# Output pattern 69:
0.1 0.9 0.1 
# Input pattern 70:
-0.293857 -1.27868 0.0804397 -0.130318 
# Output pattern 70:
0.1 0.9 0.1 
# Input pattern 71:
0.0684325 0.327318 0.590269 0.788031 
# Output pattern 71:
0.1 0.9 0.1 
# Input pattern 72:
0.309959 -0.590395 0.137087 0.132067 
# Output pattern 72:
0.1 0.9 0.1 
# Input pattern 73:
0.551486 -1.27868 0.646916 0.394453 
# Output pattern 73:
0.1 0.9 0.1 
# Input pattern 74:
0.309959 -0.590395 0.533621 0.000874618 
# Output pattern 74:
0.1 0.9 0.1 
# Input pattern 75:
0.672249 -0.360967 0.30703 0.132067 
# Output pattern 75:
0.1 0.9 0.1 
# Input pattern 76:
0.913776 -0.131539 0.363678 0.26326 
# Output pattern 76:
0.1 0.9 0.1 
# Input pattern 77:
1.1553 -0.590395 0.590269 0.26326 
# Output pattern 77:
0.1 0.9 0.1 
# Input pattern 78:
1.03454 -0.131539 0.703564 0.656838 
# Output pattern 78:
0.1 0.9 0.1 
# Input pattern 79:
0.189196 -0.360967 0.420326 0.394453 
# Output pattern 79:
0.1 0.9 0.1 
# Input pattern 80:
-0.173094 -1.04925 -0.146151 -0.261511 
# Output pattern 80:
0.1 0.9 0.1 
# Input pattern 81:
-0.414621 -1.50811 0.023792 -0.130318 
# Output pattern 81:
0.1 0.9 0.1 
# Input pattern 82:
-0.414621 -1.50811 -0.0328556 -0.261511 
# Output pattern 82:
0.1 0.9 0.1 
# Input pattern 83:
-0.0523308 -0.819823 0.0804397 0.000874618 
# Output pattern 83:
0.1 0.9 0.1 
# Input pattern 84:
0.189196 -0.819823 0.760211 0.525645 
# Output pattern 84:
0.1 0.9 0.1 
# Input pattern 85:
-0.535384 -0.131539 0.420326 0.394453 
# Output pattern 85:
0.1 0.9 0.1 
# Input pattern 86:
0.189196 0.786174 0.420326 0.525645 
# Output pattern 86:
0.1 0.9 0.1 
# Input pattern 87:
1.03454 0.0978893 0.533621 0.394453 
# Output pattern 87:
0.1 0.9 0.1 
# Input pattern 88:
0.551486 -1.73754 0.363678 0.132067 
# Output pattern 88:
0.1 0.9 0.1 
# Input pattern 89:
-0.293857 -0.131539 0.193735 0.132067 
# Output pattern 89:
0.1 0.9 0.1 
# Input pattern 90:
-0.414621 -1.27868 0.137087 0.132067 
# Output pattern 90:
0.1 0.9 0.1 
# Input pattern 91:
-0.414621 -1.04925 0.363678 0.000874618 
# Output pattern 91:
0.1 0.9 0.1 
# Input pattern 92:
0.309959 -0.131539 0.476973 0.26326 
# Output pattern 92:
0.1 0.9 0.1 
# Input pattern 93:
-0.0523308 -1.04925 0.137087 0.000874618 
# Output pattern 93:
0.1 0.9 0.1 
# Input pattern 94:
-1.01844 -1.73754 -0.259446 -0.261511 
# Output pattern 94:
0.1 0.9 0.1 
# Input pattern 95:
-0.293857 -0.819823 0.250383 0.132067 
# Output pattern 95:
0.1 0.9 0.1 
# Input pattern 96:
-0.173094 -0.131539 0.250383 0.000874618 
# Output pattern 96:
0.1 0.9 0.1 
# Input pattern 97:
-0.173094 -0.360967 0.250383 0.132067 
# Output pattern 97:
0.1 0.9 0.1 
# Input pattern 98:
0.430722 -0.360967 0.30703 0.132067 
# Output pattern 98:
0.1 0.9 0.1 
# Input pattern 99:
-0.897674 -1.27868 -0.429389 -0.130318 
# Output pattern 99:
0.1 0.9 0.1 
# Input pattern 100:
-0.173094 -0.590395 0.193735 0.132067 
# Output pattern 100:
0.1 0.9 0.1 
# Input pattern 101:
0.551486 0.556746 1.27004 1.70638 
# Output pattern 101:
0.1 0.1 0.9 
# Input pattern 102:
-0.0523308 -0.819823 0.760211 0.919223 
# Output pattern 102:
0.1 0.1 0.9 
# Input pattern 103:
1.51759 -0.131539 1.21339 1.18161 
# Output pattern 103:
0.1 0.1 0.9 
# Input pattern 104:
0.551486 -0.360967 1.04345 0.788031 
# Output pattern 104:
0.1 0.1 0.9 
# Input pattern 105:
0.793012 -0.131539 1.15675 1.3128 
# Output pattern 105:
0.1 0.1 0.9 
# Input pattern 106:
2.12141 -0.131539 1.60993 1.18161 
# Output pattern 106:
0.1 0.1 0.9 
# Input pattern 107:
-1.1392 -1.27868 0.420326 0.656838 
# Output pattern 107:
0.1 0.1 0.9 
# Input pattern 108:
1.75912 -0.360967 1.43998 0.788031 
# Output pattern 108:
0.1 0.1 0.9 
# Input pattern 109:
1.03454 -1.27868 1.15675 0.788031 
# Output pattern 109:
0.1 0.1 0.9 
# Input pattern 110:
1.63836 1.24503 1.32669 1.70638 
# Output pattern 110:
0.1 0.1 0.9 
# Input pattern 111:
0.793012 0.327318 0.760211 1.05042 
# Output pattern 111:
0.1 0.1 0.9 
# Input pattern 112:
0.672249 -0.819823 0.873507 0.919223 
# Output pattern 112:
0.1 0.1 0.9 
# Input pattern 113:
1.1553 -0.131539 0.986802 1.18161 
# Output pattern 113:
0.1 0.1 0.9 
# Input pattern 114:
-0.173094 -1.27868 0.703564 1.05042 
# Output pattern 114:
0.1 0.1 0.9 
# Input pattern 115:
-0.0523308 -0.590395 0.760211 1.57519 
# Output pattern 115:
0.1 0.1 0.9 
# Input pattern 116:
0.672249 0.327318 0.873507 1.44399 
# Output pattern 116:
0.1 0.1 0.9 
# Input pattern 117:
0.793012 -0.131539 0.986802 0.788031 
# Output pattern 117:
0.1 0.1 0.9 
# Input pattern 118:
2.24217 1.70389 1.66657 1.3128 
# Output pattern 118:
0.1 0.1 0.9 
# Input pattern 119:
2.24217 -1.04925 1.77987 1.44399 
# Output pattern 119:
0.1 0.1 0.9 
# Input pattern 120:
0.189196 -1.96696 0.703564 0.394453 
# Output pattern 120:
0.1 0.1 0.9 
# Input pattern 121:
1.27607 0.327318 1.1001 1.44399 
# Output pattern 121:
0.1 0.1 0.9 
# Input pattern 122:
-0.293857 -0.590395 0.646916 1.05042 
# Output pattern 122:
0.1 0.1 0.9 
# Input pattern 123:
2.24217 -0.590395 1.66657 1.05042 
# Output pattern 123:
0.1 0.1 0.9 
# Input pattern 124:
0.551486 -0.819823 0.646916 0.788031 
# Output pattern 124:
0.1 0.1 0.9 
# Input pattern 125:
1.03454 0.556746 1.1001 1.18161 
# Output pattern 125:
0.1 0.1 0.9 
# Input pattern 126:
1.63836 0.327318 1.27004 0.788031 
# Output pattern 126:
0.1 0.1 0.9 
# Input pattern 127:
0.430722 -0.590395 0.590269 0.788031 
# Output pattern 127:
0.1 0.1 0.9 
# Input pattern 128:
0.309959 -0.131539 0.646916 0.788031 
# Output pattern 128:
0.1 0.1 0.9 
# Input pattern 129:
0.672249 -0.590395 1.04345 1.18161 
# Output pattern 129:
0.1 0.1 0.9 
# Input pattern 130:
1.63836 -0.131539 1.15675 0.525645 
# Output pattern 130:
0.1 0.1 0.9 
# Input pattern 131:
1.87988 -0.590395 1.32669 0.919223 
# Output pattern 131:
0.1 0.1 0.9 
# Input pattern 132:
2.4837 1.70389 1.49663 1.05042 
# Output pattern 132:
0.1 0.1 0.9 
# Input pattern 133:
0.672249 -0.590395 1.04345 1.3128 
# Output pattern 133:
0.1 0.1 0.9 
# Input pattern 134:
0.551486 -0.590395 0.760211 0.394453 
# Output pattern 134:
0.1 0.1 0.9 
# Input pattern 135:
0.309959 -1.04925 1.04345 0.26326 
# Output pattern 135:
0.1 0.1 0.9 
# Input pattern 136:
2.24217 -0.131539 1.32669 1.44399 
# Output pattern 136:
0.1 0.1 0.9 
# Input pattern 137:
0.551486 0.786174 1.04345 1.57519 
# Output pattern 137:
0.1 0.1 0.9 
# Input pattern 138:
0.672249 0.0978893 0.986802 0.788031 
# Output pattern 138:
0.1 0.1 0.9 
# Input pattern 139:
0.189196 -0.131539 0.590269 0.788031 
# Output pattern 139:
0.1 0.1 0.9 
# Input pattern 140:
1.27607 0.0978893 0.930154 1.18161 
# Output pattern 140:
0.1 0.1 0.9 
# Input pattern 141:
1.03454 0.0978893 1.04345 1.57519 
# Output pattern 141:
0.1 0.1 0.9 
# Input pattern 142:
1.27607 0.0978893 0.760211 1.44399 
# Output pattern 142:
0.1 0.1 0.9 
# Input pattern 143:
-0.0523308 -0.819823 0.760211 0.919223 
# Output pattern 143:
0.1 0.1 0.9 
# Input pattern 144:
1.1553 0.327318 1.21339 1.44399 
# Output pattern 144:
0.1 0.1 0.9 
# Input pattern 145:
1.03454 0.556746 1.1001 1.70638 
# Output pattern 145:
0.1 0.1 0.9 
# Input pattern 146:
1.03454 -0.131539 0.816859 1.44399 
# Output pattern 146:
0.1 0.1 0.9 
# Input pattern 147:
0.551486 -1.27868 0.703564 0.919223 
# Output pattern 147:
0.1 0.1 0.9 
# Input pattern 148:
0.793012 -0.131539 0.816859 1.05042 
# Output pattern 148:
0.1 0.1 0.9 
# Input pattern 149:
0.430722 0.786174 0.930154 1.44399 
# Output pattern 149:
0.1 0.1 0.9 
# Input pattern 150:
0.0684325 -0.131539 0.760211 0.788031 
# Output pattern 150:
0.1 0.1 0.9 
